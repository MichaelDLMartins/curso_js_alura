var titulo = document.querySelector(".titulo"); //procura no document a class titulo
titulo.textContent = "Michael Nutrição";

var pacientes = document.querySelectorAll(".paciente"); //procura por todas as classes paciente,
														// com # procura pelo id

for (var i = 0; i < pacientes.length; i++) {
	
	var paciente = pacientes[i];

	var peso = paciente.querySelector(".info-peso").textContent;
	var altura = paciente.querySelector(".info-altura").textContent;

	var tdIMC = paciente.querySelector(".info-imc");

	var pesoValido = validaPeso(peso);
	var alturaValida = validaAltura(altura);

	if(!pesoValido){
		pesoValido = false;
		tdIMC.textContent = "Peso inválido!";
		paciente.classList.add("paciente-invalido"); //adicionado uma classe no css para alterar o fundo
													 //no javascript é adicionado pelo método classList
	}

	if(!alturaValida){
		alturaValida = false;
		tdIMC.textContent = "Altura inválido!";
		paciente.classList.add("paciente-invalido");
	}

	if(pesoValido && alturaValida){
		var imc = calcularIMC(peso,altura); // biblioteca de operações matemáticas
		tdIMC.textContent = imc; // toFixed() arredonda para quantidade de casas decimais
	}	
}

function calcularIMC(peso,altura){
	var imc = 0;

	imc = peso / Math.pow(altura,2);

	return imc.toFixed(2);
}

function validaPeso(peso){
	if(peso > 0 && peso < 1000){
		return true;
	}else{
		return false;
	}
}

function validaAltura(altura){
	if(altura >= 0 && altura < 3.0){
		return true;
	}else{
		return false;
	}
}