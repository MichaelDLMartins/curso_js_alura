var pacientes = document.querySelectorAll(".paciente");

var tabela = document.querySelector("table");

tabela.addEventListener("dblclick", function(event){
    // var alvoEvento = event.target;              
    // var paiDoEvento = alvoEvento.parentNode;    //TR = paciente = remover
    event.target.parentNode.classList.add(".fadeOut"); //adiciona a classe que exclue a linha de forma que vai sumindo
    
    setTimeout(function(){
        event.target.parentNode.remove();
    }, 500);
});